const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");

const details = require("./model.json");

const app = express();
app.use(cors({ origin: "*" }));
app.use(bodyParser.json());

app.listen(3000, () => {
  console.log("The server started on port 3000 !!!!!!");
});

app.get("/", (req, res) => {
  res.send(
    "<h1 style='text-align: center'>Wellcome to CSGI <br><br>😃👻😃👻😃👻😃👻😃</h1>"
  );
});

app.post("/sendmail", (req, res) => {
  console.log("request came");
  let user = req.body;
  sendMail(user, info => {
    console.log(`The mail has beed send 😃 and the id is ${info.messageId}`);
    res.send(info);
  }).catch(error => {
    if(error.responseCode === 553) {
      return res.status(553).send(error);
    }
    error.message = 'Enter a valid mail please.';
    res.status(500).send(error);
  });
});

async function sendMail(data, callback) {
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: details.email,
      pass: details.password
    }
  });

  let mailOptions = {
    from: 'jhoarmogui@gmail.com', // sender address
    to: data.email, // list of receivers
    subject: "✔ Open Your Mind, Quote For Smile ✔", // Subject line
    html: `<label>Hi!,<br><br>
    Hope you are having a fun day. Check this quote:<br><br>
    <b>"${data.quote}"</b><br>
    ${data.author}</p><br><br>
    Regards,<br>
    ${data.name}`
  };

  let info = await transporter.sendMail(mailOptions);

  callback(info);
}