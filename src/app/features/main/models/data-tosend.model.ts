export interface DataToSend {
  name: string;
  email: string;
  quote: string;
  author: string;
}