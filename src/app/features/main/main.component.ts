import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { QuotesService } from '../../core/services/QuotesService/quotes.service';
import { MailService } from '../../core/services/MailService/mail.service';
import { MailDialogComponent } from './mail-dialog/mail-dialog.component';
import { DataToSend } from './models/data-tosend.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit {

  private quote:string = '';
  private author:string = '';
  private loading:boolean = false;
  private dataToSend:DataToSend;
  constructor(private quotesService: QuotesService, private mailService:MailService, private dialog:MatDialog) { }

  ngOnInit() {
  	this.getQuotes();
  	//this.testy();
  }

  getQuotes() {
  	this.loading = true;
  	return this.quotesService.getQuotes()
  		.subscribe(data => {
  			this.quote = data.quote;
  			this.author = data.author;
  			this.loading = false;
  			return data;
  		});
  }
  openMailDialog() {
  	const dialogRef = this.dialog.open(MailDialogComponent, {
      width: '350px',
      data: {name: '', email: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.dataToSend = result;
        console.log('yisus',result);
        this.dataToSend.quote = this.quote;
        this.dataToSend.author = this.author;
        this.sendMail();
      }    
    });
  }
  sendMail() {
  	return this.mailService.sendEmail(this.dataToSend).subscribe(
      data => {
        let res:any = data; 
        console.log(
          `${this.dataToSend.name} is successfully register and mail has been sent and the message id is ${res.messageId}`
        );
      },
      error => {
        console.log('Error',error.error);
      }
    );
  }

}
