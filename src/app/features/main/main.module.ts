import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
//import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule, MatIconModule, MatDialogModule, MatFormFieldModule,MatInputModule } from '@angular/material';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MailDialogComponent } from './mail-dialog/mail-dialog.component';

@NgModule({
  declarations: [MainComponent,LoadingComponent, MailDialogComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule, MatIconModule, MatDialogModule,MatFormFieldModule,MatInputModule
  ],
  entryComponents:[MailDialogComponent]
})
export class MainModule { }
