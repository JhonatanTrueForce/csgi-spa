import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONSTANTS } from '../../../configs/app-constants.config';

@Injectable()

export class MailService {
  
  constructor(private http: HttpClient) { }

  sendEmail(data) {
  	return this.http.post(APP_CONSTANTS.apiUri,data);
  }
}
