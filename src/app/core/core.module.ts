import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { QuotesService } from './services/QuotesService/quotes.service';
import { MailService } from './services/MailService/mail.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
  	RouterModule,
  	HttpClientModule
  ],
  providers: [
  	QuotesService,
  	MailService
  ]
})
export class CoreModule { }
