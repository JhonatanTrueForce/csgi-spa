# CsgiSpa

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Install Dependencies

Run npm install to download all the dependencies required for the SPA.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Local Server

Run `npm run server` for a local server. This run on port:3000. The app need the local server to manage SMTP and send Mails.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
